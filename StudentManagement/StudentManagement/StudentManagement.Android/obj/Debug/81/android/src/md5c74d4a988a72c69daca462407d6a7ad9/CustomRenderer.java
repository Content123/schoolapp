package md5c74d4a988a72c69daca462407d6a7ad9;


public class CustomRenderer
	extends md51558244f76c53b6aeda52c8a337f2c37.EntryRenderer
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("StudentManagement.Droid.Custom.CustomRenderer, StudentManagement.Android", CustomRenderer.class, __md_methods);
	}


	public CustomRenderer (android.content.Context p0, android.util.AttributeSet p1, int p2)
	{
		super (p0, p1, p2);
		if (getClass () == CustomRenderer.class)
			mono.android.TypeManager.Activate ("StudentManagement.Droid.Custom.CustomRenderer, StudentManagement.Android", "Android.Content.Context, Mono.Android:Android.Util.IAttributeSet, Mono.Android:System.Int32, mscorlib", this, new java.lang.Object[] { p0, p1, p2 });
	}


	public CustomRenderer (android.content.Context p0, android.util.AttributeSet p1)
	{
		super (p0, p1);
		if (getClass () == CustomRenderer.class)
			mono.android.TypeManager.Activate ("StudentManagement.Droid.Custom.CustomRenderer, StudentManagement.Android", "Android.Content.Context, Mono.Android:Android.Util.IAttributeSet, Mono.Android", this, new java.lang.Object[] { p0, p1 });
	}


	public CustomRenderer (android.content.Context p0)
	{
		super (p0);
		if (getClass () == CustomRenderer.class)
			mono.android.TypeManager.Activate ("StudentManagement.Droid.Custom.CustomRenderer, StudentManagement.Android", "Android.Content.Context, Mono.Android", this, new java.lang.Object[] { p0 });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
