﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace StudentManagement
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SchoolMasterPage : MasterDetailPage
	{
        public List<StudentItem> studentItems { get; set; }
		public SchoolMasterPage ()
		{
			InitializeComponent ();
            studentItems = new List<StudentItem>();
            studentItems.Add(new StudentItem()
            {
                Title = "Dashboard", Image = "android.png", TargetType = typeof(Page1)

            });
            studentItems.Add(new StudentItem()
            {
                Title = "Profile",
                Image = "micro.png",
                TargetType = typeof(Page1)

            });
            studentItems.Add(new StudentItem()
            {
                Title = "Fee Collection",
                Image = "twitter.png",
                TargetType = typeof(AttendanceHolyDays)

            });

            studentItems.Add(new StudentItem()
            {
                Title = "Profile",
                Image = "micro.png",
                TargetType = typeof(Page1)

            });
            studentItems.Add(new StudentItem()
            {
                Title = "Fee Collection",
                Image = "twitter.png",
                TargetType = typeof(FeeTotal)

            });
            studentItems.Add(new StudentItem()
            {
                Title = "Leave detail",
                Image = "micro.png",
                TargetType = typeof(StudentLeave)

            });
            studentItems.Add(new StudentItem()
            {
                Title = "Student Diary",
                Image = "twitter.png",
                TargetType = typeof(Page1)

            });

            studentItems.Add(new StudentItem()
            {
                Title = "Change Password",
                Image = "micro.png",
                TargetType = typeof(ChangePassword)

            });
            studentItems.Add(new StudentItem()
            {
                Title = "Logout",
                Image = "twitter.png",
                TargetType = typeof(Page1)

            });

            navigationDrawerList.ItemsSource= studentItems;
        }

        private void NavigationDrawerList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = (StudentItem)e.SelectedItem;
            Type page = item.TargetType;
            Detail = new NavigationPage((Page)Activator.CreateInstance(page));
            IsPresented = false;

        }
    }
}