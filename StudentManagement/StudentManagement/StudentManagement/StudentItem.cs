﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StudentManagement
{
     public  class StudentItem
    {
        public string Title { get; set; }
        public string Image { get; set; }
        public Type TargetType { get; set; }
    }
}
